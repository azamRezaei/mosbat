<?php

namespace App\Models\Mosbat\V1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [ 'name','parent_id'];
    public function products()
    {
        return $this->morphedByMany(Product::class, 'categorizable');
    }
}
