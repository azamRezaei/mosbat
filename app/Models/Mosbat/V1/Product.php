<?php

namespace App\Models\Mosbat\V1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [ 'title','discription','slug','amount','current'];
    public function categories()
    {
        return $this->morphToMany(Category::class, 'categorizable');
    }
}
