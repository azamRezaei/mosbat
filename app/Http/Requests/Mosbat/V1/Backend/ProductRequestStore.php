<?php

namespace App\Http\Requests\Mosbat\V1\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Laravel\Fortify\Rules\Password;
use Laravel\Jetstream\Jetstream;

class ProductRequestStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['bail','required','unique:products' ],
            'slug' =>['bail','nullable'],
            'discription' =>['bail','nullable'],
            'amount' =>['bail','required','numeric'],
            'current' =>['bail','required','numeric'],
        ];
    }
}
