<?php

use App\Models\Mosbat\V1\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

function addPermission()
    {
        $lookFor = 'Controllers'.config('settings.options.permission');
        // dd(Route::getRoutes());
        
        foreach (Route::getRoutes() as $route) {
            $controller = $route->getAction();

            if (! isset($controller['controller'])) {
                
            }

            elseif (strpos($controller['controller'], $lookFor)) {
                $routeArray = $route->getAction();
    
                $controllerAction = class_basename($routeArray['controller']);
                $controller_permission = explode('@', $controllerAction)[0];
            
                $action = explode('@', $controllerAction)[1];
                
                $pattern='/^(.+)Controller/';
                
                preg_match($pattern, $controller_permission, $treffer);
                $permission = Permission::firstOrCreate([
                    'name' =>strtolower($treffer[1]),
                    'parentId' =>0,
                    'guard_name' => 'api'
                ]);
                
                $permissions= Permission::firstOrCreate([
                    'name' =>strtolower($treffer[1]).'-'.$action,
                    'parentId' =>$permission->id,
                    'guard_name' => 'api'
                ]);
                
            }
        }
    }
if (! function_exists('addRole')) {
    /**
     * add role.
     *
     * @param  string|array  $name
     * @param  mixed  $role
     * @param  bool  $absolute
     * @return string
     */
    function addRole()
    {
        $roles=['admin', 'user','writer'] ;
        foreach($roles as $role) {
            Role::firstOrCreate(['name' => $role]);
        }
    }
}
if (! function_exists('setPermissioAllToRollAdmin')) {
   
    function setPermissioAllToRollAdmin()
    {

        $permissions = Permission::query()->pluck('name')->toArray();
        $roleAdmin   = Role::findByName('admin') ;
        $user = User::whereType('admin')->first();
        $user->assignRole($roleAdmin);
        $user->assignRole('user');
        $roleAdmin->syncPermissions($permissions);
        $user->syncPermissions($permissions);
    }
}
if (! function_exists('checkRoleByPermission')) {
    /**
     * add role.
     *
     * @param  string|array  $name
     * @param  mixed  $role
     * @param  bool  $absolute
     * @return string
     */
    /** @var getPermissionsViaRoles */
    function checkRoleByPermission($permission)
    {
        $role = Auth::user()->getPermissionsViaRoles()->where('name',$permission)->pluck('name')->first();
        return $role;
    }
}