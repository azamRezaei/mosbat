<?php

namespace App\Http\Controllers\Mosbat\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mosbat\V1\Backend\CategoryRequestStore;
use App\Http\Requests\Mosbat\V1\Backend\CategoryRequestUpdate;
use App\Models\Mosbat\V1\Category;
use App\Repositories\Mosbat\Backend\Eloquent\Category\CategoryRepositorylnterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;
use Spatie\Permission\Models\Role;
use Throwable;

class CategoryController extends Controller
{
    private  $repositoryCategory;

    public function __construct(CategoryRepositorylnterface $repositoryCategory)
    {

        $this->repositoryCategory = $repositoryCategory;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            if(! checkRoleByPermission('category-index'))
            return Response::error('Access Denied or your message', null, 403);
            
            $category = $this->repositoryCategory->paginate(15);
            $categories = Cache::put('category',   $category->load('products'), now()->addMinutes(60));
            $categories = Cache::get('category');
            return Response::success('category list',   $categories);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CategoryRequestStore $request)
    {
        try {
            if(! checkRoleByPermission('category-store'))
            return Response::error('Access Denied or your message', null, 403);
            $Category = $this->repositoryCategory->create([
                'name' => $request->name,
                'parent_id' => ($request->parent_id) ? $request->parent_id : 0,
            ]);
            return Response::success('Category store',   $Category);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category)
    {
        try {
            if(! checkRoleByPermission('category-show'))
            return Response::error('Access Denied or your message', null, 403);
            return Response::success('Category show',   $category->load('products'));
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CategoryRequestUpdate $request, Category $category)
    {
        try {
            if(! checkRoleByPermission('category-update'))
            return Response::error('Access Denied or your message', null, 403);
            $Category = $this->repositoryCategory->update($category, $request->all());
            return Response::success('Category update',   $Category);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        try {
            if(! checkRoleByPermission('category-destroy'))
            return Response::error('Access Denied or your message', null, 403);
            $Category = $this->repositoryCategory->delete($category);
            return Response::success('Category delete',   $Category);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }
}
