<?php

namespace App\Http\Controllers\Mosbat\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mosbat\V1\Backend\RequestLogin ;
use App\Models\Mosbat\V1\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Throwable;

class LoginController extends Controller
{

    /**
     * Check login.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login(RequestLogin $request)
    {
       
        
        try {
            if (!Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])) {
            
                return response()->json([
                    'errore' => 'The information entered is incorrect.'
                ]);
            }
            
            $user =User::where('email',$request->email)->first();
            $token = $user->createToken('token-name-login', ['login:store'])->plainTextToken;
            return Response::success('Success! you are logged in successfully',   $token); 
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    public function logout() {

        Auth::user()->tokens->each(function($token, $key) {

            $token->delete();
        });
        return response()->json('Successfully logged out');
    }
}
