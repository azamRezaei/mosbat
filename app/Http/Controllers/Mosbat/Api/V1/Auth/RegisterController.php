<?php

namespace App\Http\Controllers\Mosbat\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mosbat\V1\Backend\RegisterRequest;
use App\Models\Mosbat\V1\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Throwable;

class RegisterController extends Controller
{
  

    protected function register(RegisterRequest $request)
    {
        try {
            $user= User::query()->create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
    
            ]);
            $token = $user->createToken('token-name-register', ['register:store'])->plainTextToken;
            $user->assignRole('user');
            return Response::success('Success! you are register in successfully',   $token);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

}
