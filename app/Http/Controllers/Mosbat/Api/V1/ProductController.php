<?php

namespace App\Http\Controllers\Mosbat\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mosbat\V1\Backend\ProductRequestStore;
use App\Http\Requests\Mosbat\V1\Backend\ProductRequestUpdate;
use App\Models\Mosbat\V1\Product;
use App\Repositories\Mosbat\Backend\Eloquent\Product\ProductRepositorylnterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;
use Throwable;

class ProductController extends Controller
{
    private  $repositoryProduct;
 
	public function __construct(ProductRepositorylnterface $repositoryProduct)
	{
	    $this->repositoryProduct = $repositoryProduct;
	}

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            if(! checkRoleByPermission('product-index'))
            return Response::error('Access Denied or your message', null, 403);
            $products = $this->repositoryProduct->paginate(15);
            $product = Cache::put('product',  $products->load('categories')  , now()->addMinutes(60));
            $product = Cache::get('product');
            return Response::success('Product list',   $product);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductRequestStore $request)
    {
        try {
            if(! checkRoleByPermission('product-store'))
            return Response::error('Access Denied or your message', null, 403);
            $product = $this->repositoryProduct->insertGetId([
                'title' => $request->title,
                'discription' => $request->discription,
                'amount' => $request->amount,
                'current' => $request->current,
                'slug' => ($request->slug) ? $request->parent_id : 'MOSBAT'.$request->title,
            ]);
            // morph categories
            $this->repositoryProduct->categories($product)->sync( $request->categories);
            return Response::success('product store',   $product);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        try {
            if(! checkRoleByPermission('product-show'))
            return Response::error('Access Denied or your message', null, 403);
            return Response::success('Product show',   $product->load('categories'));
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductRequestUpdate $request,Product $product)
    {
        try {
            if(! checkRoleByPermission('product-update'))
            return Response::error('Access Denied or your message', null, 403);
            $product_info = [
                'title' => $request->title,
                'discription' => $request->discription,
                'amount' => $request->amount,
                'current' => $request->current,
                'slug' => ($request->slug) ? $request->parent_id : 'MOSBAT'.$request->title,
            ];
            $products=$this->repositoryProduct->update($product,$product_info);
            // morph categories
            $this->repositoryProduct->categories($product->id)->sync( $request->categories);
            $product->load('categories');
            return Response::success('product store', $product);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
       
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        try {
            if(! checkRoleByPermission('product-destroy'))
            return Response::error('Access Denied or your message', null, 403);
            $product=$this->repositoryProduct->delete($product);
            return Response::success('product delete',   $product);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }
}
