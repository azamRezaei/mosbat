<?php

namespace App\Http\Controllers\Mosbat\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Mosbat\V1\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Spatie\Permission\Models\Role;
use Throwable;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            if(! checkRoleByPermission('role-index'))
            return Response::error('Access Denied or your message', null, 403);
            addRole();
            return Response::success('Success! you are add role in successfully',null);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            if(! checkRoleByPermission('role-store'))
            return Response::error('Access Denied or your message', null, 403);
            $user =$request->user;
            $user= User::find($user);
            $roles= $request->roles;
            foreach($roles as $role) {
                Role::firstOrCreate(['name' => $role]);
                $user->assignRole($role);
                $role->assignRole($request->permissions);
            }
            
            
            return Response::success('Success! you are add role in successfully',null);
        } catch (Throwable $e) {
            return Response::error('Access forbidden!', null, 403);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
