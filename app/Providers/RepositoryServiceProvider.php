<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
        $models = ['Category','Product'];
        foreach ($models as $idx => $model) {
            // dd("App\Repositories\Mosbat\Frontend\Eloquent\\$model\\".$model."Repository");
            $this->app->bind(
                "App\Repositories\Mosbat\Frontend\Eloquent\\$model\\".$model."Repositorylnterface", 
                "App\Repositories\Mosbat\Frontend\Eloquent\\$model\\".$model."Repository",
                
            );
            $this->app->bind(
                "App\Repositories\Mosbat\Backend\Eloquent\\$model\\".$model."Repositorylnterface", 
                "App\Repositories\Mosbat\Backend\Eloquent\\$model\\".$model."Repository"
            ); 
        }
        $this->app->bind(
            "App\Repositories\Mosbat\Frontend\Repository",
            "App\Repositories\Mosbat\Backend\Repository",
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
