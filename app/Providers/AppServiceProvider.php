<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
       
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Schema::defaultStringLength(125);
        Response::macro('success', function($message, $data) {
            return response()->json([
                'status' => 200,
                'message' => $message,
                'data' => $data,
            ]);
        });
        Response::macro('error', function($message, $data, $code = 500) {
            return response()->json([
                'status' => $code,
                'error' => $message,
                'data' => $data,
            ], $code);
        });
        
    }
}
