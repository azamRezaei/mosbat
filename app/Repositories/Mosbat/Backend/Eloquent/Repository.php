<?php

namespace App\Repositories\Mosbat\Backend\Eloquent;

abstract class Repository 
{
    protected $model;

    
    public function __construct()
    {
        $this->model = app($this->model());
    }
    abstract public function model();

    public function all()
    {
        return $this->model->orderBy('id', 'desc')->get();
    }
 
    public function paginate($limit = 15)
    {
        return $this->model->orderBy('id', 'desc')->paginate($limit);
    }
 
    public function getBy($col, $value, $limit = 15)
    {
        return $this->model->where($col, $value)->limit($limit)->get();
    }
 
    public function create(array $data)
    {
        return $this->model->create($data);
    }
    public function insertGetId(array $data)
    {
        return $this->model->insertGetId($data);
    }
 
    public function find($id)
    {
        return $this->model->find($id);
    }
 
    public function update($model, array $data)
    {
        return $model->update($data);
    }
 
    public function delete($model)
    {
        return $model->delete();
    }
 
    public function exists($id)
    {
        return $this->model->where('id', $id)->exists();
    }
    public function categories($id)
    {
        return $this->model->find($id)->categories();
    }
}