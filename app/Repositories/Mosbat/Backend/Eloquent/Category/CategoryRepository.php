<?php 
namespace App\Repositories\Mosbat\Backend\Eloquent\Category;

use App\Models\Mosbat\V1\Category;
use App\Repositories\Mosbat\Backend\Eloquent\Repository;
use App\Repositories\Mosbat\Backend\Eloquent\Category\CategoryRepositorylnterface;


class CategoryRepository extends Repository implements  CategoryRepositorylnterface
{
    public function model()
    {
        return Category::class;
    }
}
