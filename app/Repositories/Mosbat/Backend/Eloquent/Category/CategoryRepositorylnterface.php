<?php 
namespace App\Repositories\Mosbat\Backend\Eloquent\Category;


interface CategoryRepositorylnterface 
{
	public function paginate($limit);
	public function all();
	public function create(array $data);
	public function update($model, array $data);
	public function delete($model);
}