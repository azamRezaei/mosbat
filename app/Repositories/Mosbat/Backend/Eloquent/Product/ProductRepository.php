<?php 
namespace App\Repositories\Mosbat\Backend\Eloquent\Product;

use App\Models\Mosbat\V1\Product;
use App\Repositories\Mosbat\Backend\Eloquent\Repository;
use App\Repositories\Mosbat\Backend\Eloquent\Product\ProductRepositorylnterface;


class ProductRepository extends Repository implements  ProductRepositorylnterface
{
    public function model()
    {
        return Product::class;
    }
}
