<?php 
namespace App\Repositories\Mosbat\Backend\Eloquent\Product;


interface ProductRepositorylnterface 
{
	public function paginate($limit);
	public function all();
	public function create(array $data);
	public function insertGetId(array $data);
	public function update($model, array $data);
	public function delete($model);
	public function categories($id);
}