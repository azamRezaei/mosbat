<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    /**
     * A list Category unit test example.
     *
     * @return void
     */
    public function testListCategory()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ])->get('api/v1/categories');

        $response->assertStatus(200);

    }
    /**
     * A store Category unit test example.
     *
     * @return void
     */
    public function testStoreCategory()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,

        ])->post( 'api/v1/categories',[
            'name' => 'testUnit2221',
            'parent_id'=>12,

        ]);
        $response->assertStatus(200);

    }
    /**
     * A show Category unit test example.
     *
     * @return void
     */
    public function testShowCategory()
    {

        $token= env('TOKEN_IP');
        $response1 = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ])->get('api/v1/categories/12');

        $response1->assertStatus(200);

    }
    /**
     * A update Category unit test example.
     *
     * @return void
     */
    public function testUpdateCategory()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,

        ])
        ->put( 'api/v1/categories/18',
        [
            'name' => 'testUnit10',
            'parent_id'=>12,

        ]);
        $response->assertStatus(200);

    }
     /**
     * A delete Category unit test example.
     *
     * @return void
     */
    public function testDeleteCategory()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,

        ])->delete( 'api/v1/categories/19');
        $response->assertStatus(200);

    }

}
