<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    /**
     * A list Products unit test example.
     *
     * @return void
     */
    public function testListProduct()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ])->get('api/v1/products');

        $response->assertStatus(200);

    }
    /**
     * A store Product unit test example.
     *
     * @return void
     */
    public function testStoreProduct()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,

        ])->post( 'api/v1/products',[
            'title' => 'testUnit product1',
            'amount'=>2,
            'current'=>5 ,
            'discription'=>'this is a test unit',
            'categories[0]' =>50,
            'categories[1]' =>150,

        ]);
        $response->assertStatus(200);

    }
    /**
     * A show Product unit test example.
     *
     * @return void
     */
    public function testShowProduct()
    {

        $token= env('TOKEN_IP');
        $response1 = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ])->get('api/v1/products/12');

        $response1->assertStatus(200);

    }
    /**
     * A update Product unit test example.
     *
     * @return void
     */
    public function testUpdateProduct()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,

        ])
        ->put( 'api/v1/products/10',
        [
        'title' => 'testUnit22',
        'amount'=>10,
        'current'=>10,
        'discription'=>'test',
        'categories[0]' =>50,
        'categories[0]' =>150,

        ]);
        $response->assertStatus(200);

    }
     /**
     * A delete Product unit test example.
     *
     * @return void
     */
    public function testDeleteProduct()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,

        ])->delete( 'api/v1/products/19');
        $response->assertStatus(200);

    }

}
