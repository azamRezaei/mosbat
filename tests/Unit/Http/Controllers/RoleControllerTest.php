<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;

class RoleControllerTest extends TestCase
{

    public function testListRoles()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ])->get('api/v1/roles');

        $response->assertStatus(200);

    }
     

}
