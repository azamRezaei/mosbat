<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;

class PermissionControllerTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

        public function testListPermission()
    {

        $token= env('TOKEN_IP');
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ])->get('api/v1/permissions');

        $response->assertStatus(200);

    }
    

}
