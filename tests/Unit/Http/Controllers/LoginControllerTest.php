<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testSuccessfulLogin()
    {
        $loginData = ['email' => 'admin@gmail.com', 'password' => '12345678'];

        $this->json('POST', 'api/v1/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure();

        $this->assertAuthenticated('web');
        
    }
    public function testSuccessfulLogout()
    {
        $loginData = ['email' => 'admin@gmail.com', 'password' => '12345678'];

        $this->json('POST', 'api/v1/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure();

        $this->assertAuthenticated('web');
    }
}
