<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;

class RegisterControllerTest extends TestCase
{
    /**
     * A basic unit test Validation Errors Registration.
     *
     * @return void
     */
    public function testRequiredFieldsForRegistration()
    {
        $this->postJson('api/v1/register', ['Accept' => 'application/json'])
            ->assertStatus(422)
           ->assertJsonValidationErrors(['name','email','password'], $responseKey = 'errors');
    }
    /**
     * A basic unit test test  Successful  Registration.
     *
     * @return void
     */
    public function testSuccessfulRegistration()
    {
        $userData = [
            "name" => "admin1366",
            "password" => "12345678",
            "email" => "admin1366@gmail.com",
        ];

       
        $this->json('POST', 'api/v1/register',  $userData, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure();

   
    }
}
