<?php

use App\Http\Controllers\Mosbat\Api\V1\Auth\LoginController;
use App\Http\Controllers\Mosbat\Api\V1\Auth\RegisterController;
use App\Http\Controllers\Mosbat\Api\V1\CategoryController;
use App\Http\Controllers\Mosbat\Api\V1\PermissionController;
use App\Http\Controllers\Mosbat\Api\V1\ProductController;
use App\Http\Controllers\Mosbat\Api\V1\RoleController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/register', [RegisterController::class, 'register']);
});

 Route::middleware('auth:sanctum')->prefix('v1')->group(function () {
    Route::post('/logout', [LoginController::class, 'logout']);
    Route::resource('/categories', CategoryController::class);
    Route::resource('/products', ProductController::class);
    Route::resource('/permissions', PermissionController::class);
    Route::resource('/roles', RoleController::class);
    
    
 });








































































































