<?php

namespace Database\Factories\Mosbat\V1;

use App\Models\Mosbat\V1\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Product::class;
    public function definition(): array
    {
        return [
            'title' => fake()->unique()->name(),
            'discription' =>fake()->text(150),
            'slug' => fake()->unique()->name().Str::random(10),
            'amount' => rand(100,200),
            'current' => rand(5,15),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
