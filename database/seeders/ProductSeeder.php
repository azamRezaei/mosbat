<?php

namespace Database\Seeders;

use App\Models\Mosbat\V1\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Product::factory(15)->create();
        Product::find(1)->categories()->sync([1,2,3,12]);
        Product::find(5)->categories()->sync([7,8,9,12]);
        Product::find(6)->categories()->sync([1,2,12]);

    }
}
